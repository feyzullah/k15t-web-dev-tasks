var userTable = function () {
  function getJSONdata (url) {
    console.log('Write the result from ' + url + ' into the #datatable')
    var Httpreq = new XMLHttpRequest() // a new request
    Httpreq.open('GET', url, false)
    Httpreq.send(null)
    return Httpreq.responseText
  }

  var json = getJSONdata('https://jsonplaceholder.typicode.com/users')

  var obj = JSON.parse(json)

  console.log($('table'))

  var userData = function (user) {
      $('table tbody').append('<tr><td>'+ user.username +'</td><td><a href="'+ user.email +'">'+ user.email +'</a></td><td>'+ user.address.street +'</td><td><a target="_blank" href="https://www.google.de/maps/place/'+user.address.geo.lat+','+user.address.geo.lng+'">'+ user.address.city +'</a></td><td>'+ user.company.name +'</td><td>'+ user.company.catchPhrase +'</td></tr>');
  };

  obj.forEach(userData);
};

$(function(){
  userTable();
});
